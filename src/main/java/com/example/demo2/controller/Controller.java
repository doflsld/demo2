package com.example.demo2.controller;

import com.example.demo2.domain.entity.Test;
import com.example.demo2.domain.entity.Testm;
import com.example.demo2.dto.TestmDto;
import com.example.demo2.dto.TestsDto;
import com.example.demo2.service.TestService;
import com.example.demo2.service.TestmService;
import com.example.demo2.service.TestsService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@org.springframework.stereotype.Controller
public class Controller {

    private TestsService testsService;
    private TestmService testmService;
    private TestService testService;

    public Controller(TestsService testsService, TestmService testmService, TestService testService){
        this.testsService = testsService;
        this.testmService = testmService;
        this.testService = testService;
    }
    @GetMapping("/info_bat")
    public String info_bat(){ return "info_bat"; }

    @GetMapping("/monitor")
    public String monitor(Model model){
        Long test0 = testService.getTest(0);
        model.addAttribute("test0", test0);
        Long test1 = testService.getTest(1);
        model.addAttribute("test1", test1);
        Long test2 = testService.getTest(2);
        model.addAttribute("test2", test2);
        return "monitor";
    }

    @GetMapping("/main")
    public String main(Model model) {
        TestmDto testmDto = testmService.getTestm();
        model.addAttribute("testm", testmDto);
        return "index";
    }

    @GetMapping("/info")
    public String info(Model model){
        TestsDto testsDto = testsService.getTests();
        model.addAttribute("tests", testsDto);
        TestmDto testmDto = testmService.getTestm();
        model.addAttribute("testm", testmDto);
        return "index_info";
    }
    @GetMapping("/")
    public String index(Model model){
        TestmDto testmDto = testmService.getTestm();
        model.addAttribute("testm", testmDto);
        return "index";
    }

    @RequestMapping(value = "/data_in")
    public String getdata(@RequestParam(name="key1") String val1, @RequestParam(name="key2") Integer val2){
        Test test = new Test();
        test.setBat_num(val1);
        test.setStatus(val2);
        testService.save(test);
        System.out.println(val1);
        System.out.println(val2);
        return "datain";
    }

}

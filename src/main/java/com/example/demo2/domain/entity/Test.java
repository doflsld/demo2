package com.example.demo2.domain.entity;

import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name="test")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    private String bat_num;

    @Column
    private Integer status;

    @Builder
    public Test(Long id, String bat_num, Integer status){
        this.id = id;
        this.bat_num = bat_num;
        this.status = status;
    }

}

package com.example.demo2.dto;

import com.example.demo2.domain.entity.Test;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TestDto {
    private Long id;
    private String bat_num;
    private Integer status;

    public Test toEntity(){
        Test build = Test.builder()
                .id(id)
                .bat_num(bat_num)
                .status(status)
                .build();
        return build;
    }

    @Builder
    public TestDto(Long id, String bat_num, Integer status){
        this.id = id;
        this.bat_num = bat_num;
        this.status = status;
    }

}

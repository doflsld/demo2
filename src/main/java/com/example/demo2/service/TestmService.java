package com.example.demo2.service;

import com.example.demo2.domain.entity.Testm;
import com.example.demo2.domain.entity.Tests;
import com.example.demo2.domain.repository.TestmRepository;
import com.example.demo2.dto.TestmDto;
import com.example.demo2.dto.TestsDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TestmService {
    private TestmRepository testmRepository;

    public TestmService(TestmRepository testmRepository){
        this.testmRepository = testmRepository;
    }

    @Transactional
//    @Scheduled(cron = "5 * * * * *")
    public TestmDto getTestm(){

        Testm testm = testmRepository.findAllByOrderByIdDesc().get(0);

        TestmDto testmDto = TestmDto.builder()
                .id(testm.getId())
                .lat(testm.getLat())
                .lng(testm.getLng())
                .build();

        return testmDto;
    }

}

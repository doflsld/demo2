package com.example.demo2.service;

import com.example.demo2.domain.entity.Test;
import com.example.demo2.domain.repository.TestRepository;
import org.springframework.stereotype.Service;

@Service
public class TestService {
    private TestRepository testRepository;

    public TestService(TestRepository testRepository){
        this.testRepository = testRepository;
    }
    public void save(Test test){
        testRepository.save(test);
    }

    public Long getTest(Integer status){
        Long test = testRepository.countByStatus(status);
        return test;
    }

}
